class Product{
    constructor(productNumber, description, price, fee){
        this.productNumber = productNumber;
        this.description = description;
        this.price = price;
        this.fee = fee;
    }

    totalAmount(){
        return this.price + this.fee;
    }

}


var product1 = new Product("p010", "product1", 200, 20);
var product2 = new Product("p020", "product2", 100, 0);
var product3 = new Product("p100", "product3", 100, 5);
var product4 = new Product("p200", "product4", 200, 0);
var product5 = new Product("p300", "product5", 300, 10);

var products = [product1, product2, product3, product4, product5];

function printProductDescriptions(goods){
    var strList = "<ul>";
    for (var i = 0; i < goods.length; i++){
        strList += "<li>" + goods[i].description + "</li>";
    }
    strList += "</ul>";
    return strList;
}

function printProductPrices(prices){
    var strList = "<ol>";
    for (var i = 0; i < prices.length; i++){
        strList += "<li>" + prices[i].price + "</li>";
    }
    strList += "</ol>";
    return strList;
}
document.getElementById("ulist").innerHTML = printProductDescriptions(products);
document.getElementById("olist").innerHTML = printProductPrices(products);