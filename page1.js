function showCurrentTime(){
    var d = new Date();
    var month = d.getMonth() + 1;
    var day  = d.getDate();
    var year = d.getFullYear();
    var hour = d.getHours();
    var minute = d.getMinutes();
    document.getElementById("paragraph-1-1").innerHTML = "<b>Current Date and Time on Your Computer : " + 
    month + "/" + day + "/" + year + " at " + hour + ":" + minute + "<b>";
}
showCurrentTime();

var product1 = {
    productNumber:"p010",
    description:"product 1",
    price:200,
    fee:20,
    totalAmount: function(){
        return this.price + this.fee;
    }

};

var product2 = {
    productNumber:"p020",
    description:"product 2",
    price:"100",
    fee: 0,
    totalAmount: function(){
        return this.price + this.fee;
    }

};

function Product(productNumber, description, price, fee){
    this.productNumber = productNumber;
    this.description = description;
    this.price = price;
    this.fee = fee;
    this.totalAmount = function(){
        return this.price + this.fee;
    };
}
var product3 = new Product("p100", "product 3", 100, 5);
var product4 = new Product("p200", "product 4", 200, 0);
var product5 = new Product("p300", "product 5", 300, 10);
var product6 = new Product("p400", "product 6", 150, 0);

var products = [];
products.push(product1);
products.push(product2);
products.push(product3);
products.push(product4);
products.push(product5);
products.push(product6);

function productionTable(goods){
    var result;
    result = '<table border = "1"><th>Product Number</th><th>Description</th><th>Price</th><th>Fee</th><th>Total</th>';
    for(i = 0; i < goods.length; i++){
        result += "<tr>";
        result += "<td>" + goods[i].productNumber + "</td>";
        result += "<td>" + goods[i].description + "</td>";
        result += "<td>" + goods[i].price + "</td>";
        result += "<td>" + goods[i].fee + "</td>";
        result += "<td>" + goods[i].totalAmount() + "</td>";
        result += "</tr>";
     }
     result += "</table>";

     return result;

}

document.getElementById("part2").innerHTML = productionTable(products);